import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import Comment from './Comment';
import { UserContext } from '../../UserContext';
import { useContext } from 'react';
import ListComment from './ListComment';
import Rate from './Rate';

function BlogDetail(){
    let params = useParams();
    const [getB_Detail, setB_Detail] = useState('');
    const [comment, setComment] = useState([]);


    

    useEffect(()=> {
      axios.get('http://localhost/laravel/public/api/blog/detail/' + params.id)
      .then(response => {
        setB_Detail(response.data.data);
        setComment(response.data.data.comment);

        // console.log(response.data.data)
      })
      .catch(function (error) {
          console.log(error)
      })
    },[])

    function getCom(ttCom){
      setComment(comment.concat(ttCom))
    }

    function fetchDetail(){
      if(Object.keys(getB_Detail).length > 0){
        return (
          <>
          <div className="blog-post-area">
            <h2 className="title text-center">Latest From our Blog</h2>
            <div className="single-blog-post">
              <h3>{getB_Detail.title}</h3>
              <div className="post-meta">
                <ul>
                  <li><i className="fa fa-user" /> Mac Doe</li>
                  {/* <li><i className="fa fa-clock-o" /> 1:33 pm</li> */}
                  <li><i className="fa fa-calendar" />{getB_Detail.created_at}</li>
                </ul>
              </div>
              <a href>
                <img src={"http://localhost/laravel/public/upload/Blog/image/" + getB_Detail.image} />
              </a>
              <p>
               {getB_Detail.description}
              </p>
              <div className="pager-area">
                <ul className="pager pull-right">
                  <li><a href="#">Pre</a></li>
                  <li><a href="#">Next</a></li>
                </ul>
              </div>
            </div>
          </div>{/*/blog-post-area*/}
          </>
        )
      }

    }






    return(
        <>
        
      <div className="col-sm-9">
        {fetchDetail()}
        <div className="rating-area">
          <ul className="ratings">
            <li className="rate-this">Rate this item:</li>
            <Rate id_blog={params.id}/>  
          </ul>
          <ul className="tag">
            <li>TAG:</li>
            <li><a className="color" href>Pink <span>/</span></a></li>
            <li><a className="color" href>T-Shirt <span>/</span></a></li>
            <li><a className="color" href>Girls</a></li>
          </ul>
        </div>{/*/rating-area*/}
        <div className="socials-share">
          <a href><img src="images/blog/socials.png" alt="" /></a>
        </div>{/*/socials-share*/}

        <ListComment comment={comment} />
        <Comment id_blog={params.id} getCom={getCom}/>
      </div>

        </>
    )



}

export default BlogDetail;