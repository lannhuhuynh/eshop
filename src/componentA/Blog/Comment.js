import React, { useState } from 'react';
import axios from "axios";
import { UserContext } from '../../UserContext';
import { useContext } from 'react';

function Comment(props){
    const [comment, setComment] = useState("");
    const idRL = useContext(UserContext);
    const idReply = idRL.ttin
    // console.log(idRL.ttin)

    function handleComment(e){
        setComment(e.target.value);
    }

    const postComment = () => {
        let ttLogin = localStorage.getItem('ttLogin');
        if(ttLogin){
            if(comment == ""){
                alert("Vui long nhap binh luan")
            }else{
                ttLogin = JSON.parse(ttLogin);

                let url = 'http://localhost/laravel/public/api/blog/comment/' + props.id_blog
                let accessToken = ttLogin.data.success.token;
                

                let config = { 
                    headers: { 
                    'Authorization': 'Bearer '+ accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                    } 
                }

                const formData = new FormData();
                formData.append('id_blog', props.id_blog);
                formData.append('id_user', ttLogin.data.Auth.id);
                formData.append('id_comment', idReply ? idReply : 0);
                formData.append('comment', comment);
                formData.append('image_user', ttLogin.data.Auth.avatar);
                formData.append('name_user', ttLogin.data.Auth.name);
                
      
            axios.post(url, formData, config)
            .then(response => {
              console.log(response.data.data)

                props.getCom(response.data.data)
            })
            setComment('')
            }


    }}




    return (
    

        <div className="replay-box">
          <div className="row">
            <div className="col-sm-12">
              <h2>Leave a replay</h2>
              <div className="text-area">
                <div className="blank-arrow">
                  <label>Your Name</label>
                </div>
                <span>*</span>
                <textarea value={comment} onChange={handleComment} name="message" rows={11} defaultValue={""} />
                <a onClick={postComment} className="btn btn-primary" href>Post Comment</a>
              </div>
            </div>
          </div>
        </div>
          
      )

}

export default Comment;
