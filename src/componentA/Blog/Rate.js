import React, { useState , useEffect } from 'react';
import StarRatings from 'react-star-ratings';
import axios from 'axios';

function Rate(props){
        

    const [rating, setRating] = useState(0);
    const [SLrate,setSLrate] = useState(0)

    useEffect(()=> {
        axios.get('http://localhost/laravel/public/api/blog/rate/' + props.id_blog)
        .then(response => {
        
            // console.log(response.data.data)
          if(Object.keys(response.data.data).length > 0){
            let tong = 0;
            Object.keys(response.data.data).map((key, index) => {
              tong = tong + response.data.data[key]['rate'] 
            })
            setRating(tong/Object.keys(response.data.data).length)
            setSLrate(Object.keys(response.data.data).length)
          }

        })
        .catch(function (error) {
            console.log(error)
        })
    },[])

    function changeRating( newRating, name ) {
        setRating(newRating)

        let ttLogin = localStorage.getItem('ttLogin')
            if(ttLogin){
                ttLogin = JSON.parse(ttLogin);

                let url = 'http://localhost/laravel/public/api/blog/rate/' + props.id_blog
                let accessToken = ttLogin.data.success.token;
                

                let config = { 
                    headers: { 
                    'Authorization': 'Bearer '+ accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                    } 
                };
                

                    const formData = new FormData();
                        formData.append('blog_id', props.id_blog);
                        formData.append('user_id', ttLogin.data.Auth.id);
                        formData.append('rate', rating);
                        
              
                    axios.post(url, formData, config)
                    .then(response => {
                      console.log(response)
                    })

            }else{
                alert("Vui long login account")

            }
    }
    
  
    
        

    return(
        <>
            <StarRatings
                rating={rating}
                starRatedColor="#FE980F"
                changeRating={changeRating}
                numberOfStars={5}
                name='rating'
            />
            
            <li className="color">({SLrate} votes)</li>
        </>
    )

}

export default Rate;