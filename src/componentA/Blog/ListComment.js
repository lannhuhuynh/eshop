import React, {useState} from 'react';
import { useContext } from 'react';
import { UserContext } from '../../UserContext';

function ListComment(props){
    // console.log(props.comment)
    const abc = useContext(UserContext);

    function replay(e){
        abc.Getdata(e.target.id)

    }
    
    function fetchListCom(){
        if(Object.keys(props.comment).length > 0){
            return props.comment.map(function(value, key1) {
                if(value.id_comment == 0){
                    return(
                    
                      <React.Fragment key={key1}>
  
                      <li className="media">
                          <a className="pull-left" href="#" >
                              <img className="media-object" src={"http://localhost/laravel/public/upload/user/avatar/" + value.image_user} />
                          </a>
                          <div className="media-body">
                              <ul className="sinlge-post-meta">
                              <li><i className="fa fa-user" />{value.name_user}</li>
                              {/* <li><i className="fa fa-clock-o" /> 1:33 pm</li> */}
                              <li><i className="fa fa-calendar" /> {value.created_at}</li>
                              </ul>
                              <p>{value.comment}</p>
                              <a onClick={replay} id={value.id} className="btn btn-primary" href><i className="fa fa-reply" />Replay</a>
                          </div>
                      </li>
                        
                        {props.comment.map((value2, key2) => {
                          if(value2.id_comment == value.id ){
                            return (
                              <li className="media second-media" key={key2}>
                                <a className="pull-left" href="#">
                                  <img className="media-object" src={"http://localhost/laravel/public/upload/user/avatar/" + value2.image_user} />
                                </a>
                                <div className="media-body">
                                  <ul className="sinlge-post-meta">
                                    <li><i className="fa fa-user" />{value2.name_user}</li>
                                    {/* <li><i className="fa fa-clock-o" /> 1:33 pm</li> */}
                                    <li><i className="fa fa-calendar" /> {value.created_at}</li>
                                  </ul>
                                  <p>{value2.comment}</p>
                                  <a className="btn btn-primary" href><i className="fa fa-reply" />Replay</a>
                                </div>
                              </li>
              
                            )
                          }
  
                        })}
  
                      </React.Fragment>
      
                      )
  
                  };

            })
        }

    }
    return(
        <>
        <div className="response-area">
        <h2>{(props.comment).length} RESPONSES</h2>
            <ul className="media-list">
            {fetchListCom()}
            </ul>
        </div>{/*/Response-area*/}
        </>
    )
}

export default ListComment;
