import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';


function BlogList(){
    const [getbloglist, setbloglist] = useState('')

    useEffect(()=> {
      axios.get('http://localhost/laravel/public/api/blog')
      .then(response => {
        setbloglist(response.data.blog.data)
        // console.log(response.data.blog.data)
      })
      .catch(function (error) {
          console.log(error)
      })
    },[])

    function fetchBlogList(){
        if (Object.keys(getbloglist).length > 0){
            return getbloglist.map((value, key) => {
                return(
                    <div className="single-blog-post">
                        <h3>{value.title}</h3>
                        <div className="post-meta">
                        <ul>
                            <li><i className="fa fa-user" /> Mac Doe</li>
                            {/* <li><i className="fa fa-clock-o" /> 1:33 pm</li> */}
                            <li><i className="fa fa-calendar" />{value.created_at}</li>
                        </ul>
                        <span>
                            <i className="fa fa-star" />
                            <i className="fa fa-star" />
                            <i className="fa fa-star" />
                            <i className="fa fa-star" />
                            <i className="fa fa-star-half-o" />
                        </span>
                        </div>
                        <a href>
                        <img src={"http://localhost/laravel/public/upload/Blog/image/" + value.image} />
                        </a>
                        <p>{value.description}</p>
                        <Link to={"/blog/detail/" + value.id} className="btn btn-primary" href>Read More</Link>
                    </div>

                )
            })
        }

    }


    return(
        <div className="col-sm-9">
        <div className="blog-post-area">
          <h2 className="title text-center">Latest From our Blog</h2>
          {fetchBlogList()}


          <div className="pagination-area">
            <ul className="pagination">
              <li><a href className="active">1</a></li>
              <li><a href>2</a></li>
              <li><a href>3</a></li>
              <li><a href><i className="fa fa-angle-double-right" /></a></li>
            </ul>
          </div>
        </div>
      </div>

    )
}


export default BlogList;