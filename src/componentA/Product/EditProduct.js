import { useState , useEffect} from "react";
import React from "react";
import axios from "axios";
import {useNavigate} from "react-router-dom"
import ErrorForm from "../ErrorForm";

function EditProduct(){
    const navigate = useNavigate();
    const [inputs, setInputs] = useState({
        name:"",
        price:"",
        category:"",
        brand:"",
        company:"",
        detail:"",
        status: 0,
        sale:"",
        image:"",
        id_user:""
        
    });

    const handleInput = (e) => {
        const nameInput = e.target.name;
        const value = e.target.value;
        setInputs(state => ({...state,[nameInput]:value}));
    };

    const [getCategory, setCategory] = useState('');
    const [getBrand, setBrand] = useState('');
    const [file, setFile] = useState();
    const [errors, setErrors] = useState({});
    const [hinhxoa, setHinhxoa] = useState([]);

    useEffect(()=> {
        axios.get('http://localhost/laravel/public/api/category-brand')
        .then(response => {
          // console.log(response.data)
          setCategory(response.data.category)
          setBrand(response.data.brand)
  
        })
        .catch(function (error) {
            console.log(error)
        })
    },[])

    function Option_Category() {
        return Object.keys(getCategory).map((key, index) => {
            return (
                <option value={getCategory[key]['id']} key={index}>{getCategory[key]['category']}</option>
                
            )}
        ) 
      } 
  
    function Option_Brand() {
        return Object.keys(getBrand).map((key, index) => {
            return (
                <option value={getBrand[key]['id']} key={index}>{getBrand[key]['brand']}</option>
            )}
        ) 
    }

    function hanldeFile(e){
        setFile(e.target.files)
        // console.log(e.target.files)
    }
    
    function Percent_Sale(){
        if(inputs.status == 1) {
          return (
            <>
            <input className="col-sm-4" style={{width: '150px'}} type="text" placeholder="0" name="sale" onChange={handleInput} value={inputs.sale}/>
            <span>%</span>
            </>
          )
        }
    }

    let ttLogin = localStorage.getItem('ttLogin')
    ttLogin = JSON.parse(ttLogin);




    let accessToken = ttLogin.data.success.token;
    

    let config = { 
        headers: { 
        'Authorization': 'Bearer '+ accessToken,
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
        } 
    };
    
    let ttProductId = localStorage.getItem('ttProductId')
    ttProductId = JSON.parse(ttProductId);

    useEffect(()=> {
        axios.get('http://localhost/laravel/public/api/user/product/' + ttProductId, config)
        .then(response => {
          // setProduct(response.data.data)
          console.log(response.data.data)
          setInputs({
              name: response.data.data.name,
              price: response.data.data.price,
              category: response.data.data.id_category,
              brand: response.data.data.id_brand,
              company: response.data.data.company_profile,
              detail: response.data.data.detail,
              status: response.data.data.status,
              sale: response.data.data.sale ? response.data.data.sale : '',
              image: response.data.data.image,
              id_user: response.data.data.id_user
  
          })
        })
        .catch(function (error) {
            console.log(error)
        })
    },[])

    function handleCheck(e){
        const isChecked = e.target.checked;

        

        if(isChecked){
         
          setHinhxoa(state => ([...state,e.target.name]));

          
        }else{
            const filtered = hinhxoa.filter(name => name != e.target.name)
            setHinhxoa(filtered)
        }
    }

    function Pic(){

        if(inputs.image){
            return inputs.image.map((key, index) => {

                return (
                    <>

                    <td key={index}>
                    
                    <a href><img style={{width: '100px'}} src={"http://localhost/laravel/public/upload/user/product/" + inputs.id_user + "/" + inputs.image[index]} /></a>
                    
                    <input type="checkbox" name={inputs.image[index]}  onChange={handleCheck} />
                    
                    </td>
                    
                    </>
                )   
            })  
        }
    }

    
    function handleSubmit(e) {
        e.preventDefault();
        let errorSubmit ={};
        let flag = true;
  
  
        if(inputs.name == ""){
            flag = false;
            errorSubmit.name = "Vui long nhap Name"
        };
  
        if(inputs.price == ""){
            flag = false;
            errorSubmit.password = "Vui long nhap Price"
        };
  
        if(inputs.category == ""){
            flag = false;
            errorSubmit.phone = "Vui long chon Category"
        };
  
        if(inputs.brand == ""){
            flag = false;
            errorSubmit.address = "Vui long chon Brand"
        };
  
        if(inputs.company == ""){
          flag = false;
          errorSubmit.address = "Vui long nhap Company profile"
        };
  
        if(inputs.detail == ""){
          flag = false;
          errorSubmit.address = "Vui long nhap Detail"
        };
        
        if(inputs.status == 0){
          inputs.sale = ''
        }
        if(inputs.status == "1" && inputs.sale == ""){
          flag = false;
          errorSubmit.sale = "Vui long nhap % sale"
  
        }

        const sohinhcongiu = (inputs.image).length - hinhxoa.length;

        if(sohinhcongiu == 0 && file == undefined ){
          flag = false;            
          errorSubmit.img = "Vui long upload hinh"

        }
  
        if(file == undefined) {
            flag = false;            
            errorSubmit.img = "Vui long upload hinh"
  
        }else{
          Object.keys(file).map((item, i) => {
    
      
            const duoifile = ['jpeg', 'png', 'jpg', 'PNG' , 'JPG'];
              
            const hinh = file[item]['name'].split('.');
            const fileext=hinh[1];
            const imgtype= duoifile.includes(fileext)
    
            if((file[item]['size']) > 1024 * 1024 || !imgtype) {
                     
            
            errorSubmit.sizeimg = "Loi: Type img khong dung hoac Size img > 1mb"
            flag = false;
    
            }
    
            
          })

        }


        if (file != undefined && ((file.length) + sohinhcongiu ) > 3) {
          flag = false;            
          errorSubmit.imglength = "Chi duoc upload toi da 3 hinh"
        }
  
  
        if(!flag){
            setErrors(errorSubmit);
        }else{
            setErrors("");
  
            let ttLogin = localStorage.getItem('ttLogin')
            ttLogin = JSON.parse(ttLogin);
  
  
            let url = 'http://localhost/laravel/public/api/user/edit-product/' + ttProductId
            let accessToken = ttLogin.data.success.token;
            
  
            let config = { 
                headers: { 
                'Authorization': 'Bearer '+ accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
                } 
            };
            
  
                const formData = new FormData();
                    formData.append('name', inputs.name);
                    formData.append('price', inputs.price);
                    formData.append('category', inputs.category);
                    formData.append('brand', inputs.brand);
                    formData.append('company', inputs.company);
                    formData.append('detail', inputs.detail);
                    formData.append('status', inputs.status);
                    formData.append('sale', inputs.sale ? inputs.sale : '');

                    if(file){
                      Object.keys(file).map((item, i) => {
                        formData.append("file[]", file[item]);
                      })

                    }

                    if(hinhxoa){
                      Object.keys(hinhxoa).map((item, i) => {
                        formData.append("avatarCheckBox[]", hinhxoa[item]);
                      })
                    }
                    

                axios.post(url, formData, config)
                .then(response => {
                    console.log(response)
                    navigate("/account/myproduct")

                })
        
    };
  
    } 







  return(
    <>

    <div className="col-sm-7">
      <div className="signup-form">
        <h2>Edit Product</h2>
        <form onSubmit={handleSubmit} encType="multipart/form-data">
          <input type="text" placeholder="Name" name="name" onChange={handleInput} value={inputs.name} />
          <input type="text" placeholder="Price" name="price" onChange={handleInput} value={inputs.price} />
          <select name="category" onChange={handleInput} value={inputs.category} >
            {Option_Category()}
          </select>

          <select name="brand" onChange={handleInput} value={inputs.brand} >
            {Option_Brand()}
          </select>

          <select name="status" onChange={handleInput} value={inputs.status} >
            <option value="0">New</option>
            <option value="1">Sale</option>
          </select>
          {Percent_Sale()}

          <input type="text" placeholder="Company profile" name="company" onChange={handleInput} value={inputs.company} />

          <input type="file" id="files" name="image" multiple onChange={hanldeFile} />

          {Pic()}

          <textarea  placeholder="Detail"  name="detail" onChange={handleInput} value={inputs.detail}  />

          <button type="submit" className="btn btn-default">EDIT</button>

          <ErrorForm errors={errors} />
          
          
        </form>
      </div>
    </div>
      </>
  )

}

export default EditProduct;