import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { UserContext } from '../../UserContext';

function WishList(){
    const [WL_item, setWL_item] = useState('');
    const abc = useContext(UserContext)

    useEffect(()=> {
        let SLyt = localStorage.getItem('item_wishlist');
        if(SLyt){
          SLyt = JSON.parse(SLyt)
          const SLyt2 = SLyt.map(Number);
          
          axios.get('http://localhost/laravel/public/api/product/wishlist')
          .then(response => {
            const filtered = response.data.data.filter(item => SLyt2.includes(item.id))
            setWL_item(filtered);

          })
          .catch(function (error) {
              console.log(error)
          })
    
        }
    
    },[])

    function addcart(e){
  
        const idItemcart = e.target.id;
        let flag = true;
        let IC = {}
    
          
      let xx = localStorage.getItem('item_cart');
      
    
        if(xx){
            IC = JSON.parse(xx)
        
        
            Object.keys(IC).map((key, value) =>{
            
            if(idItemcart==key){
                IC[key] += 1;
                flag = false;
        
            }
        
        })}
    
        if(flag==true){
          IC[idItemcart] = 1;
        }

        let tongqty = 0;
        Object.keys(IC).map(function(key, index){
            tongqty = tongqty + IC[key]
        })

        abc.Getdata2(tongqty)
    
      localStorage.setItem('item_cart', JSON.stringify(IC));
      
        
    }

    function remove_WL(e){
        const id_remove = e.target.id;
        // console.log(id_remove)
    
        let newWL = [...WL_item]
        let newWL2=[];
    
        newWL.map(function(value, key){
      
                  
          if(id_remove == newWL[key]['id']){
            
      
            // newWL.splice((newWL.findIndex(item => item.id == id_remove)), 1)
            // newWL = newWL.filter(val => val[key]['id'] != id_remove)
            newWL2 = newWL.filter(item => item.id != id_remove)
    
            console.log(newWL2)
    
    
            // const indexOfObject = newWL.findIndex(object => {
            //   return object.id == id_remove;
            // });
    
            // console.log(indexOfObject)
      
            // if (indexOfObject !== -1) {
            //   newWL.splice(indexOfObject, 1);
              
            // }
      
            
              
          }  
        })
        setWL_item(newWL2)
        let tongconWL = newWL2.length;
        
        abc.Getdata3(tongconWL)
      
        let ItemWL = localStorage.getItem('item_wishlist');
        
        if(ItemWL){
          ItemWL = JSON.parse(ItemWL)
      
          ItemWL.map(function(value, key){
                    
            if(id_remove==value){
              ItemWL = ItemWL.filter(val => val !== id_remove)
            }
            console.log(ItemWL)  
           localStorage.setItem('item_wishlist', JSON.stringify(ItemWL));
      
        });
      
        }
    
    }

    function Wish_items () {
        if(Object.keys(WL_item).length > 0) {
            return WL_item.map((value, key) => {
              let hinh = JSON.parse(value.image);      
              return (
                <>
                      <div className="col-sm-4" key={key}>
                        <div className="product-image-wrapper">
                          <div className="single-products">
                            <div className="productinfo text-center">
                              <img src={"http://localhost/laravel/public/upload/user/product/" + value.id_user + "/" + hinh[0]} />
                              <h2>${value.price}</h2>
                              <p>{value.name}</p>
                              <a className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                            </div>
                            <div className="product-overlay">
                              <div className="overlay-content">
                                <h2>${value.price}</h2>
                                <p>{value.name}</p>
                                <a onClick={addcart} id={value.id} className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</a>
                              </div>
                            </div>
                          </div>
                          <div className="choose">
                            <ul className="nav nav-pills nav-justified">
                              <li><a onClick={remove_WL} id={value.id}><i className="fa fa-times" />Remove item on wishlist</a></li>
                              <li><Link to={"/product/detail/" + value.id}><i className="fa fa-plus-square" />See product details</Link></li>
                          
    
                            </ul>
                          </div>
                        </div>
                      </div>
                </>
      
                
              )
            }) 
        }
    }
  

    return(
        <>
    <div className="col-sm-9 padding-right">
      <div className="features_items">
        <h2 className="title text-center">WishList's Items</h2>
        {Wish_items()}

      </div>
    </div>
        </>
    )

}

export default WishList;