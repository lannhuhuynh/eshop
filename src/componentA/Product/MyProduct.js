import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

function MyProduct(){
    const [getProduct, setProduct] = useState('');

    let ttLogin = localStorage.getItem('ttLogin')
    ttLogin = JSON.parse(ttLogin);


    let accessToken = ttLogin.data.success.token;
    

    let config = { 
        headers: { 
        'Authorization': 'Bearer '+ accessToken,
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
        } 
    };
    

    useEffect(()=> {
      axios.get('http://localhost/laravel/public/api/user/my-product', config)
      .then(response => {
        setProduct(response.data.data)
        // console.log(response.data.data)
      })
      .catch(function (error) {
          console.log(error)
      })
    },[])

    function My_Product() {
        if(Object.keys(getProduct).length > 0) {
           
            return Object.keys(getProduct).map((key, index) => {
            let hinh = JSON.parse(getProduct[key]['image']);


              return (
            <tr key={index}>
            <td className="cart_description">
                <h5><a href>{getProduct[key]['id']}</a></h5>
              </td>

              <td className="cart_description">
                <h5><a href>{getProduct[key]['name']}</a></h5>
              </td>

              <td className="cart_product">
                <a href><img className='myproduct_hinh_thunho' src={"http://localhost/laravel/public/upload/user/product/" + getProduct[key]['id_user'] + "/" + hinh[0]} /></a>
              </td>

              <td className="cart_price">
                <p><span>$</span>{getProduct[key]['price']}</p>
              </td>


              <td className="cart_description">
                <Link to='/account/editproduct' onClick={EditProduct} id={getProduct[key]['id']} style={{color: '#428bca'}} href><i className="fa fa-pencil-square-o" /></Link>
                
              </td> 

              <td className="cart_description">
                <a onClick={DeleteProduct} id={getProduct[key]['id']} style={{color: '#428bca'}} ><i className="fa fa-times" /></a>
              </td> 
             </tr>

              )
            }) 
        }
    }

    function DeleteProduct(e){
        e.preventDefault();
        const idDel = e.currentTarget.id;
        // console.log(idDel)

        let ttLogin = localStorage.getItem('ttLogin')
        ttLogin = JSON.parse(ttLogin);
  
        let accessToken = ttLogin.data.success.token;
        
  
        let config = { 
            headers: { 
            'Authorization': 'Bearer '+ accessToken,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
            } 
        };
        
        axios.get('http://localhost/laravel/public/api/user/delete-product/' + idDel , config)
        .then(response => {
          setProduct(response.data.data)
          console.log(response.data.data)
        })
        .catch(function (error) {
            console.log(error)
        })

    }

    function EditProduct(e){
        const id_Edit = e.currentTarget.id;

        localStorage.setItem('ttProductId', JSON.stringify(id_Edit))
    }

    return(
        <>
      <div  className="table-responsive cart_info  col-sm-7">
        <table className="table table-condensed">
          <thead style={{background: '#FE980F'}}>
            <tr style={{color: 'white'}}  className="cart_menu">
                <td className="description">ID</td>
                <td className="description">Name</td>
                <td className="image">Image</td>
                <td className="price">Price</td>
                <td className="description">Action</td>
              <td />
            </tr>
          </thead>
          <tbody>
          {My_Product()}
          </tbody>
        </table>
        <Link to='/account/addproduct'><button type="submit" className=" nutaddnew btn btn-default">Add New</button></Link>
      </div>
        </>
    )

}

export default MyProduct;