import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {useNavigate} from "react-router-dom"
import ErrorForm from '../ErrorForm';

function AddProduct() {
    const navigate = useNavigate();

    const [inputs, setInputs] = useState({
        name:"",
        price:"",
        category:"",
        brand:"",
        company:"",
        detail:"",
        status:"",
        sale:"",
    });

    const [getCategory, setCategory] = useState('');
    const [getBrand, setBrand] = useState('');
    const [file, setFile] = useState();
    const [errors, setErrors] = useState({});

    useEffect(()=> {
        axios.get('http://localhost/laravel/public/api/category-brand')
        .then(response => {
          // console.log(response.data)
          setCategory(response.data.category)
          setBrand(response.data.brand)
  
        })
        .catch(function (error) {
            console.log(error)
        })
    },[])

    const handleInput = (e) => {
        const nameInput = e.target.name;
        const value = e.target.value;
        setInputs(state => ({...state,[nameInput]:value}));
    };

    function Option_Category() {
      
        return Object.keys(getCategory).map((key, index) => {
            return (
                <option value={getCategory[key]['id']} key={index}>{getCategory[key]['category']}</option>
            )}
    )} 

    function Option_Brand() {
    
        return Object.keys(getBrand).map((key, index) => {
            return (
            <option value={getBrand[key]['id']} key={index}>{getBrand[key]['brand']}</option>
            
        )}
    )}

    function hanldeFile(e){
        setFile(e.target.files)
        // console.log(e.target.files)
    }

    function Percent_Sale(){
        if(inputs.status == 1) {
          return (
            <>
            <input className="col-sm-4" style={{width: '150px'}} type="text" placeholder="0" name="sale" onChange={handleInput} />
            <span>%</span>
            </>

          )
        }
    }

    function handleSubmit(e) {

        e.preventDefault();
        let errorSubmit ={};
        let flag = true;
  
  
        if(inputs.name == ""){
            flag = false;
            errorSubmit.name = "Vui long nhap Name"
        };
  
        if(inputs.price == ""){
            flag = false;
            errorSubmit.password = "Vui long nhap Price"
        };
  
        if(inputs.category == ""){
            flag = false;
            errorSubmit.phone = "Vui long chon Category"
        };
  
        if(inputs.brand == ""){
            flag = false;
            errorSubmit.address = "Vui long chon Brand"
        };
  
        if(inputs.company == ""){
          flag = false;
          errorSubmit.address = "Vui long nhap Company profile"
        };
  
        if(inputs.detail == ""){
          flag = false;
          errorSubmit.address = "Vui long nhap Detail"
        };
  
        if(inputs.status == 0){
          inputs.sale = ""
        }
  
  
        if(inputs.status == ""){
          flag = false;
          errorSubmit.status = "Vui long chon Status"
        }else if(inputs.status == "1" && inputs.sale == ""){
          flag = false;
          errorSubmit.sale = "Vui long nhap % sale"
  
        }
  
  
        if(file == undefined) {
            flag = false;            
            errorSubmit.img = "Vui long upload hinh"
  
        }
        else if (file.length > 3) {
          flag = false;            
          errorSubmit.imglength = "Chi duoc upload toi da 3 hinh"
        }
        else{
  
            Object.keys(file).map((item, i) => {
    
      
              const duoifile = ['jpeg', 'png', 'jpg', 'PNG' , 'JPG'];
                
              const hinh = file[item]['name'].split('.');
              const fileext=hinh[1];
              const imgtype= duoifile.includes(fileext)
      
              if((file[item]['size']) > 1024 * 1024 || !imgtype) {
                       
              
              errorSubmit.sizeimg = "Loi: Type img khong dung hoac Size img > 1mb"
              flag = false;
      
              }
      
              
            })
  
        }
  
        if(!flag){
            setErrors(errorSubmit);
        }else{
            setErrors("");
  
            let ttLogin = localStorage.getItem('ttLogin')
            ttLogin = JSON.parse(ttLogin);
  
  
            let url = 'http://localhost/laravel/public/api/user/add-product'
            let accessToken = ttLogin.data.success.token;
            
  
            let config = { 
                headers: { 
                'Authorization': 'Bearer '+ accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
                } 
            };
            
  
                const formData = new FormData();
                    formData.append('name', inputs.name);
                    formData.append('price', inputs.price);
                    formData.append('category', inputs.category);
                    formData.append('brand', inputs.brand);
                    formData.append('company', inputs.company);
                    formData.append('detail', inputs.detail);
                    formData.append('status', inputs.status);
                    formData.append('sale', inputs.sale ? inputs.sale : '');
  
                    Object.keys(file).map((item, i) => {
                      formData.append("file[]", file[item]);
                    })
  
  
              
                axios.post(url, formData, config)
                .then(response => {
                    console.log(response)
                    navigate("/account/myproduct")
  
            })
        
    };
  
  
  
  
    } 


  


    

    return(
        <>
    <div className="col-sm-7">
        <div className="signup-form">
          <h2>Create Product</h2>
          <form onSubmit={handleSubmit} encType="multipart/form-data">
            <input type="text" placeholder="Name" name="name" onChange={handleInput} />
            <input type="text" placeholder="Price" name="price" onChange={handleInput}/>
            <select name="category" onChange={handleInput}>
              <option value="">Please choose category</option>
              {Option_Category()}
            </select>

            <select name="brand" onChange={handleInput}>
              <option value="">Please choose brand</option>
              {Option_Brand()}
            </select>

            <select name="status" onChange={handleInput}>
              <option value="">Please choose Status</option>
              <option value="0">New</option>
              <option value="1">Sale</option>
            </select>
            {Percent_Sale()}

            <input type="text" placeholder="Company profile" name="company" onChange={handleInput}/>

            <input type="file" id="files" name="image" multiple onChange={hanldeFile}/>

            <textarea placeholder="Detail" name="detail" onChange={handleInput}  />

            <button type="submit" className="btn btn-default">ADD</button>

            <ErrorForm errors={errors} />
          </form>
        </div>
    </div>
        
        </>
    )

}

export default AddProduct;