import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { UserContext } from '../../UserContext';

function Cart(){

    const [getItemcart, setItemcart] = useState([]);
    let tongCTD = 0;
    const abc = useContext(UserContext)


    useEffect(()=> {
        let Itemcart = localStorage.getItem('item_cart');
        if(Itemcart){
        Itemcart = JSON.parse(Itemcart)
          axios.post("http://localhost/laravel/public/api/product/cart", Itemcart)
          .then((res) => {
              console.log(res.data.data)
              setItemcart(res.data.data)
    
          })
     
        }
    },[])

    function up_qty(e){
        e.preventDefault();
        const id_up = e.target.id;

        let newObj = [...getItemcart]
        newObj.map(function(value, key){ 
            if(id_up == newObj[key]['id']){
    
              newObj[key]['qty'] += 1;  
              
            }  
        })
        setItemcart(newObj)

        let Itemcart = localStorage.getItem('item_cart');
  
        if(Itemcart){
            Itemcart = JSON.parse(Itemcart)
            Object.keys(Itemcart).map(function(key, index){
                    
              if(id_up == key){
                Itemcart[key] = Itemcart[key] + 1
              }
           localStorage.setItem('item_cart', JSON.stringify(Itemcart));
        });}
        
    }

    function down_qty(e){
        e.preventDefault();
        const id_down = e.target.id;


        let newObj = [...getItemcart]
        newObj.map(function(value, key){ 
            if(id_down == newObj[key]['id'] && newObj[key]['qty'] > 1){
    
              newObj[key]['qty'] -= 1;  
              
            }else if(id_down == newObj[key]['id'] && newObj[key]['qty'] == 1){

                const indexOfObject = newObj.findIndex(object => {
                    return object.id == id_down;
                  });
            
                  if (indexOfObject !== -1) {
                    newObj.splice(indexOfObject, 1);
                    
                  }

            }
            console.log(newObj)
              
        })
        setItemcart(newObj)
        

        let Itemcart = localStorage.getItem('item_cart');
  
        if(Itemcart){
            Itemcart = JSON.parse(Itemcart)
            Object.keys(Itemcart).map(function(key, index){
                    
              if(id_down == key && Itemcart[key] > 1){
                Itemcart[key] = Itemcart[key] - 1
              }else if(id_down == key && Itemcart[key] == 1){
                delete Itemcart[id_down]
              }
           localStorage.setItem('item_cart', JSON.stringify(Itemcart));
        });}
        
    }

    function delete_cart(e){
        e.preventDefault();
        const id_del = e.currentTarget.id;
        let newObj3 = [];

        let newObj = [...getItemcart]
        newObj.map(function(value, key){
            if(id_del == newObj[key]['id']){
                newObj3 = newObj.filter(item => item.id != id_del)
            }
        })
        setItemcart(newObj3)

        let Itemcart = localStorage.getItem('item_cart');
  
        if(Itemcart){
            Itemcart = JSON.parse(Itemcart)
            Object.keys(Itemcart).map(function(key, index){
                    
              if(id_del == key){
                delete Itemcart[id_del]
              }
           localStorage.setItem('item_cart', JSON.stringify(Itemcart));
        });}

        newObj.map(function(value, key){
            tongCTD = tongCTD + newObj[key]['qty']
        
        })
        abc.Getdata2(tongCTD)




        
    }

    function total_chg(){
        let totalC = 0;

        if(Object.keys(getItemcart).length > 0){
            getItemcart.map((value, key) => {
                const tong = (getItemcart[key]['qty'] * getItemcart[key]['price'])
                totalC = totalC + tong;
                tongCTD = tongCTD + getItemcart[key]['qty']
            })
            abc.Getdata2(tongCTD)
            return(totalC)

        }else{return(0)}

    }

    function Item_Cart(){
        if(Object.keys(getItemcart).length > 0){
            return getItemcart.map((value, key)=>{
                let hinh = JSON.parse(value.image);
                let tong = (getItemcart[key]['qty'] * getItemcart[key]['price'])
    
                return(
                    <>
                    <tr>
                      <td className="cart_product">
                        <a href><img img style={{width: '200px'}} src={"http://localhost/laravel/public/upload/user/product/" + value.id_user + "/" + hinh[0]} /></a>
                      </td>
                      <td className="cart_description">
                        <h4><a href>{value.name}</a></h4>
                        <p>ID: {value.id}</p>
                      </td>
                      <td className="cart_price">
                        <p>${value.price}</p>
                      </td>
                      <td className="cart_quantity">
                        <div className="cart_quantity_button">
                          <a onClick={up_qty} id={value.id} className="cart_quantity_up" href> + </a>
                          <input className="cart_quantity_input" type="text" name="quantity" autoComplete="off" size={2} value={value.qty}/>
                          <a onClick={down_qty} id={value.id} className="cart_quantity_down" href> - </a>
                        </div>
                      </td>
                      <td className="cart_total">
                        <p className="cart_total_price">${tong}</p>
                      </td>
                      <td className="cart_delete">
                        <a onClick={delete_cart} id={value.id} className="cart_quantity_delete" href><i className="fa fa-times" /></a>
                      </td>
                    </tr>
                    </>
                )
            })
    
        }

    }






    return(
        <>
    <section id="cart_items">
        <div className="container">
          <div className="breadcrumbs">
            <ol className="breadcrumb">
              <li><a href="#">Home</a></li>
              <li className="active">Shopping Cart</li>
            </ol>
          </div>
          <div className="table-responsive cart_info">
            <table className="table table-condensed">
              <thead>
                <tr className="cart_menu">
                  <td className="image">Item</td>
                  <td className="description" />
                  <td className="price">Price</td>
                  <td className="quantity">Quantity</td>
                  <td className="total">Total</td>
                  <td />
                </tr>
              </thead>
              <tbody>
                {Item_Cart()}
              </tbody>
            </table>
          </div>
        </div>
    </section> {/*/#cart_items*/}

    <section id="do_action">
        <div className="container">
          <div className="heading">
            <h3>What would you like to do next?</h3>
            <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
          </div>
          <div className="row">
            <div className="col-sm-6">
              <div className="chose_area">
                <ul className="user_option">
                  <li>
                    <input type="checkbox" />
                    <label>Use Coupon Code</label>
                  </li>
                  <li>
                    <input type="checkbox" />
                    <label>Use Gift Voucher</label>
                  </li>
                  <li>
                    <input type="checkbox" />
                    <label>Estimate Shipping &amp; Taxes</label>
                  </li>
                </ul>
                <ul className="user_info">
                  <li className="single_field">
                    <label>Country:</label>
                    <select>
                      <option>United States</option>
                      <option>Bangladesh</option>
                      <option>UK</option>
                      <option>India</option>
                      <option>Pakistan</option>
                      <option>Ucrane</option>
                      <option>Canada</option>
                      <option>Dubai</option>
                    </select>
                  </li>
                  <li className="single_field">
                    <label>Region / State:</label>
                    <select>
                      <option>Select</option>
                      <option>Dhaka</option>
                      <option>London</option>
                      <option>Dillih</option>
                      <option>Lahore</option>
                      <option>Alaska</option>
                      <option>Canada</option>
                      <option>Dubai</option>
                    </select>
                  </li>
                  <li className="single_field zip-field">
                    <label>Zip Code:</label>
                    <input type="text" />
                  </li>
                </ul>
                <a className="btn btn-default update" href>Get Quotes</a>
                <a className="btn btn-default check_out" href>Continue</a>
              </div>
            </div>
            <div className="col-sm-6">
              <div className="total_area">
                <ul>
                  <li>Cart Sub Total <span>$59</span></li>
                  <li>Eco Tax <span>$2</span></li>
                  <li>Shipping Cost <span>Free</span></li>
                  <li>Total <span>${total_chg()}</span></li>
                </ul>
                <a className="btn btn-default update" href>Update</a>
                <a className="btn btn-default check_out" href>Check Out</a>
              </div>
            </div>
          </div>
        </div>
      </section>
        </>
    )

}

export default Cart;
