import axios from "axios";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import ErrorForm from "../ErrorForm";

function Login(){
  const navigate = useNavigate();

  const [inputs, setInputs] = useState({
    email:"",
    password:""
  });

  const [errors, setErrors] = useState({});

  function handleInput(e){
    const nameInput = e.target.name;
    const value = e.target.value;
    setInputs(state => ({...state,[nameInput]:value}))
  }

  function handleSubmit(e){
      e.preventDefault();
      let errorSubmit={};
      let flag = true;

      if(inputs.email == ""){
        flag = false;
        errorSubmit.email = 'Vui long nhap email'
      }else{
        let re = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!re.test(inputs.email)) {
          errorSubmit.email = "Email khong hop le";
          flag = false;
        }
      }

      if(inputs.password == ""){
        flag = false;
        errorSubmit.password = "Vui long nhap password"
      };

      if(!flag){
        setErrors(errorSubmit);
      }else{
        setErrors('');

        const data = {
          email: inputs.email,
          password: inputs.password,
          level: 0
        }
        
        axios.post("http://localhost/laravel/public/api/login", data)
        .then((res)=>{
          if(res.data.errors){
            setErrors(res.data.errors)
          }else{
            alert("Login thanh cong");
            navigate("/")

            localStorage.setItem('login',JSON.stringify(true));
            console.log(res)

            // console.log(res.data.Auth)
            // console.log(res.data.success.token)
            localStorage.setItem('ttLogin', JSON.stringify(res))
          }
        })
      }
      
      
  }


    return(
        <>
              <div className="login-form">{/*login form*/}
                <h2>Login to your account</h2>
                <form action="#" onSubmit={handleSubmit}>
                  <input type="email" placeholder="Email Address" name="email" onChange={handleInput}/>
                  <input type="password" placeholder="Password" name="password" onChange={handleInput}/>

                  <span>
                    <input type="checkbox" className="checkbox" /> 
                    Keep me signed in
                  </span>
                  <button type="submit" className="btn btn-default">Login</button>
                </form>
                <ErrorForm errors = {errors}/>
              </div>{/*/login form*/}
        </>
    )

}

export default Login;