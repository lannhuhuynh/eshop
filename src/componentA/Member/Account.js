import { useState , useEffect} from "react";
import React from "react";
import ErrorForm from "../ErrorForm";
import axios from "axios";


function Account(){
    const [errors, setErrors] = useState({});
    const [file, setFile] = useState('');
    const [avatar, setAvatar] = useState();

    const [inputs, setInputs] = useState({
        name: "",
        email:"",
        password:"",
        phone:"",
        address:"",
        id: ""

    });

    useEffect(()=> {
        let ttLogin = localStorage.getItem('ttLogin')
   
        if(ttLogin){
            ttLogin = JSON.parse(ttLogin);
            setInputs({
                name: ttLogin.data.Auth.name,
                email: ttLogin.data.Auth.email,
                password: ttLogin.data.Auth.password,
                phone: ttLogin.data.Auth.phone,
                address: ttLogin.data.Auth.address,
                id: ttLogin.data.Auth.id,
                level: ttLogin.data.Auth.level

            })
        }
    },[])

    const handleInput = (e) => {
        const nameInput = e.target.name;
        const value = e.target.value;
        setInputs(state => ({...state,[nameInput]:value}));
    };

    function hanldeFile(e){

        const file = e.target.files;
        let reader = new FileReader();

        reader.onload = (e) => {
            setAvatar(e.target.result);
            setFile(file[0]);
        }

        reader.readAsDataURL(file[0]);

    }

    function handleSubmit(e){

        e.preventDefault();
        let errorSubmit ={};
        let flag = true;


        if(inputs.name == ""){
            flag = false;
            errorSubmit.name = "Vui long nhap name"
        };


        if(inputs.password == ""){
            flag = false;
            errorSubmit.password = "Vui long nhap password"
        };

        if(inputs.phone == ""){
            flag = false;
            errorSubmit.phone = "Vui long nhap so dien thoai"
        };

        if(inputs.address == ""){
            flag = false;
            errorSubmit.address = "Vui long nhap address"
        };

        if(file != '') {
 
            const duoifile = ['jpeg', 'png', 'jpg', 'PNG' , 'JPG'];
            
            const hinh = file['name'].split('.');
            const fileext=hinh[1];
            const imgtype= duoifile.includes(fileext)

            if((file['size']) > 1024 * 1024 || !imgtype) {
                     
            
            errorSubmit.sizeimg = "Loi: Type img khong dung hoac Size img > 1mb"
            flag = false;

            }

        }

        if(!flag){
            setErrors(errorSubmit);
        }else{
            setErrors("");


            let ttLogin = localStorage.getItem('ttLogin')
            ttLogin = JSON.parse(ttLogin);




            let url = 'http://localhost/laravel/public/api/user/update/' + inputs.id
                let accessToken = ttLogin.data.success.token;
                

                let config = { 
                    headers: { 
                    'Authorization': 'Bearer '+ accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                    } 
                };

                const formData = new FormData();
                    formData.append('name', inputs.name);
                    formData.append('password', inputs.password ? inputs.password : "");
                    formData.append('email', inputs.email);
                    formData.append('phone', inputs.phone);
                    formData.append('address', inputs.address);
                    formData.append('level', 0);
                    if(avatar){
                        formData.append('avatar', avatar);
                    }

              
                axios.post(url, formData, config)
                .then(response => {
                    // console.log(response.data.Auth)
                     localStorage.setItem('ttLogin', JSON.stringify(response))
                     alert('Successfully Updated')

                })
        };

    }

    return(
        <>
    <div className="col-sm-5 col-sm-offset-4">
    <div className="signup-form">
      <h2>User Update</h2>
      <form onSubmit={handleSubmit} enctype="multipart/form-data">
        <input type="text" placeholder="Name" name="name" onChange={handleInput} value={inputs.name}/>
        <input type="email" placeholder="Email Address" name="email" onChange={handleInput}  value={inputs.email} readonly="readonly"/>
        <input type="password" placeholder="Password" name="password" onChange={handleInput} value=''/>
        <input type="text" placeholder="Phone" name="phone" onChange={handleInput} value={inputs.phone} />
        <input type="text" placeholder="Address" name="address"  onChange={handleInput} value={inputs.address}/>
        <input type="file"  onChange={hanldeFile}/>

        <button type="submit" className="btn btn-default">Signup</button>
        <ErrorForm errors={errors} />
      </form>
    </div>
    </div>
        </>
    )
}

export default Account;