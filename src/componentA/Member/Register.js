import axios from "axios";
import React, { useState } from "react";
import ErrorForm from "../ErrorForm";

function Register(){
    const [inputs, setInputs] = useState({
        name:"",
        email:"",
        password:"",
        phone:"",
        address:"",

    });

    const [errors, setErrors] = useState({});
    const [file, setFile] = useState();
    const [Avatar, setAvatar] = useState();
    
    const handleInput = (e) => {
        const nameInput = e.target.name;
        const value = e.target.value;
        setInputs(state => ({...state,[nameInput]:value}));
    };

    function hanldeFile(e){
        const file = e.target.files;
        let reader = new FileReader();

        reader.onload = (e) => {
            setAvatar(e.target.result);
            setFile(file[0]);
        }

        reader.readAsDataURL(file[0]);
    }

    function handleSubmit(e){
        e.preventDefault();
        let errorSubmit ={};
        let flag = true;

        if(inputs.name == ""){
            flag = false;
            errorSubmit.name = "Vui long nhap name"
        };

        if(inputs.email == ""){
            flag = false;
            errorSubmit.email = "Vui long nhap email"
        }else {
            let re = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      
            if (!re.test(inputs.email) ) {
                errorSubmit.email = "Email khong hop le";
                flag = false;
            }
      
        };

        if(inputs.password == ""){
            flag = false;
            errorSubmit.password = "Vui long nhap password"
        };

        if(inputs.phone == ""){
            flag = false;
            errorSubmit.phone = "Vui long nhap so dien thoai"
        };

        if(inputs.address == ""){
            flag = false;
            errorSubmit.address = "Vui long nhap address"
        };


        if(file == undefined) {
            flag = false;            
            errorSubmit.img = "Vui long upload hinh avatar"

        }
        else{
            const duoifile = ['jpeg', 'png', 'jpg', 'PNG' , 'JPG'];
            
            const hinh = file['name'].split('.');
            const fileext=hinh[1];
            const imgtype= duoifile.includes(fileext)

            if((file['size']) > 1024 * 1024 || !imgtype) {
            errorSubmit.sizeimg = "Loi: Type img khong dung hoac Size img > 1mb"
            flag = false;

            }
        }

        if(!flag){
            setErrors(errorSubmit);
        }else{
            setErrors("");

            const data = {
                name: inputs.name,
                email: inputs.email,
                password: inputs.password,
                phone: inputs.phone,
                address: inputs.address,
                avatar: Avatar,
                level: 0
            }

            axios.post("http://localhost/laravel/public/api/register", data)
            .then((res) => {
                console.log(res)
                alert('Dang ky thanh cong')
                if(res.data.errors){
                    setErrors(res.data.errors)
                }
            }
            )
            
            
        };
    }


    return(
        <>
              <div className="signup-form">{/*sign up form*/}
                <h2>New User Signup!</h2>
                <form onSubmit={handleSubmit} action="#" enctype="multipart/form-data">
                    <input type="text" placeholder="Name" name="name" onChange={handleInput} />
                    <input type="email" placeholder="Email Address" name="email" onChange={handleInput}/>
                    <input type="password" placeholder="Password" name="password" onChange={handleInput}/>
                    <input type="text" placeholder="Phone" name="phone" onChange={handleInput}/>
                    <input type="text" placeholder="Address" name="address" onChange={handleInput} />
                    <input type="file" placeholder="Avatar" onChange={hanldeFile}/>
                    <input type="text" placeholder="Level" />

                  <button type="submit" className="btn btn-default">Signup</button>
                </form>
                <ErrorForm errors ={errors} />
              </div>{/*/sign up form*/}
        </>
    )

}

export default Register;