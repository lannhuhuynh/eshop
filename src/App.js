import logo from './logo.svg';
import './App.css';

import React, { useState } from 'react';
import { useLocation } from "react-router-dom"
import { UserContext } from './UserContext';
import Footer from './componentA/Footer';
import Header from './componentA/Header';
import MenuLeft from './componentA/MenuLeft';
import MenuLeft2 from './componentA/MenuLeft2';




function App(props) {
  let param1 = useLocation();
  const [ttin, setTtin] = useState('')
  const [ttCart, setCart] = useState('')
  const [ttinW, setttinW] = useState('')



  function Getdata(abc){
    setTtin(abc)
  }
  function Getdata2(xyz){
    setCart(xyz);
    localStorage.setItem('item_tongqty', xyz);
  }

  
  function Getdata3(aa){
    setttinW(aa)
    localStorage.setItem('qty_wish', aa);

  }


  return (
    <>
    <UserContext.Provider value={{
      Getdata:Getdata,
      ttin:ttin,
      Getdata2:Getdata2,
      ttCart:ttCart,
      Getdata3:Getdata3,
      ttinW:ttinW
    }}>
    <Header />
      <section>
        <div className="container">
          <div className="row">
              {param1['pathname'].includes("account")  ? <MenuLeft2/> : ( param1['pathname'].includes("cart") || param1['pathname'].includes("login") ? "" :  <MenuLeft />) }

              {props.children}
            
          </div>
        </div>
      </section>
      <Footer />

    </UserContext.Provider>
    </>
  );
}

export default App;
