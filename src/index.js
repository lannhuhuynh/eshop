import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";
import BlogList from './componentA/Blog/BlogList';
import Home from './componentA/Home';
import BlogDetail from './componentA/Blog/BlogDetail';
import R_L from './componentA/Member/R_L';
import Account from './componentA/Member/Account';
import AddProduct from './componentA/Product/AddProduct';
import MyProduct from './componentA/Product/MyProduct';
import EditProduct from './componentA/Product/EditProduct';
import ProductDetails from './componentA/Product/ProductDetail';
import Cart from './componentA/Product/Cart';
import WishList from './componentA/Product/WishList.js';



const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Router>
      <App>
        <Routes>
          <Route index path='/' element={<Home />} /> 

          <Route path='/blog/list' element={<BlogList />} />
          <Route path='/blog/detail/:id' element={<BlogDetail />} /> 

          <Route path='/login' element={<R_L />} /> 
          <Route path='/account' element={<Account />} /> 

          <Route path='/account/addproduct' element={<AddProduct />} /> 
          <Route path='/account/myproduct' element={<MyProduct />} /> 
          <Route path='/account/editproduct' element={<EditProduct />} /> 
          <Route  path='/product/detail/:id' element={<ProductDetails />} />
          <Route path='/cart' element={<Cart />} /> 
          <Route  path='/wishlist' element={<WishList/>} /> 
          






          

          

        </Routes>
      </App>
    </Router>
    
    
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
